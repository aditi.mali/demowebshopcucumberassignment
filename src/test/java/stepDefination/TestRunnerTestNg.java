package stepDefination;


import io.cucumber.testng.AbstractTestNGCucumberTests;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources",glue= {"stepDefination"},plugin={"json:target/DestinationReport/cucumber.json",
														"html:target/DestinationReport/report.html"})

public class TestRunnerTestNg extends AbstractTestNGCucumberTests {
	
	
	
	

}
