package stepDefination;

import org.openqa.selenium.WebDriver;

import baseclasses.BaseClass;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pomclasses.BookPage;
import pomclasses.ElectronicsPage;
import pomclasses.GiftCard;
import pomclasses.LoginPage;
import pomclasses.LogoutPage;

public class StepDefination {
	
	WebDriver driver;

	LoginPage login;
	BookPage books;
	ElectronicsPage electronic;
	GiftCard gift;
	LogoutPage logout;
	
	@Given("user is on application home page")
	public void user_is_on_application_home_page() {
	   
		driver=BaseClass.getWebdriver();
	    
	}

	@When("click on the login button")
	public void click_on_the_login_button() {
	   
		login= new LoginPage(driver);
		login.clickLogin();
	   
	}

	@When("user enter the email {string} in email field")
	public void user_enter_the_email_in_email_field(String string) {
	    
		login= new LoginPage(driver);
		login.enterEmail(string);
	    
	}

	@When("user enter the password {string} in password field")
	public void user_enter_the_password_in_password_field(String string) {
		
		login= new LoginPage(driver);
		login.enterPassword(string);
	   
	}

	@Then("click on submit button")
	public void click_on_submit_button() {
		
		login= new LoginPage(driver);
		login.submitLogin();
	    
	}

	@Then("user should be at home page")
	public void user_should_be_at_home_page() {
	   

		System.out.println("User should be land on homepage");
	   
	}

	@When("Click on books")
	public void click_on_books() {
	    
		books = new BookPage(driver);
		books.clickBooks();
	 
	}

	@When("Select price from High to low")
	public void select_price_from_high_to_low() {
	   
		books = new BookPage(driver);
		books.selectBook();
	  
	}

	@When("Add any two books in cart")
	public void add_any_two_books_in_cart() throws InterruptedException {
	  
		books = new BookPage(driver);
		books.addBook();
	   
	}

	@Then("user able to see the successfully added to cart message")
	public void user_able_to_see_the_successfully_added_to_cart_message() {
	   
		System.out.println("user add to cart Page display successfully");
	    
	}

	@When("Click on electronics Category")
	public void click_on_electronics_category() {
	    
		electronic= new ElectronicsPage(driver);
		electronic.clickElectronics();
	
	 
	}

	@When("Select the Cell phones")
	public void select_the_cell_phones() {
	   
		electronic= new ElectronicsPage(driver);
		electronic.clickCellPhone();
	    
	}

	@When("Add to cart")
	public void add_to_cart() throws InterruptedException {
	   
		electronic= new ElectronicsPage(driver);
		electronic.clickSmartPhone();
		
	}

	@When("Display the count of items added to the cart.")
	public void display_the_count_of_items_added_to_the_cart() {
	  
		electronic= new ElectronicsPage(driver);
		electronic.showCartDetails();
	    
	}

	@Then("Click on GiftCart")
	public void click_on_gift_cart() {
	   
		gift= new GiftCard(driver);
		gift.clickGiftCard();
	
	}

	@Then("Select Display Four per page")
	public void select_display_four_per_page() {
	
		gift= new GiftCard(driver);
		gift.selectGiftCard();
		
	   
	}

	@Then("Capture the name and price of one of the  Gift cards displayed")
	public void capture_the_name_and_price_of_one_of_the_gift_cards_displayed() {
	    
		gift= new GiftCard(driver);
		gift.displayGiftCart();
	   
	}

	@Then("Click on Logout and capture login Diplayed on Home page")
	public void click_on_logout_and_capture_login_diplayed_on_home_page() {
	   
		logout= new LogoutPage(driver);
		logout.clickLogout();
	  
	}

}
