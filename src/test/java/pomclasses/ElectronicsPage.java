package pomclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ElectronicsPage {
	
	WebDriver driver;
	
	@FindBy(partialLinkText="Electronics")
	private WebElement clickelectronics;
	
	@FindBy(partialLinkText="Cell phones")
	private WebElement clickcellphone;
	
	@FindBy(linkText="Smartphone")
	private WebElement clicksmartphone;
	
	@FindBy(id="add-to-cart-button-43")
	private WebElement addtocart;
	
	@FindBy(xpath="//span[@class='cart-qty']")
	private WebElement showcartproduct;
	

	public ElectronicsPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	public void clickElectronics()
	{
		clickelectronics.click();
	}
	
	public void clickCellPhone()
	{
		clickcellphone.click();
	}
	
	public void clickSmartPhone() throws InterruptedException
	{
		clicksmartphone.click();
		addtocart.click();
		Thread.sleep(3000);
	}
	
	public void showCartDetails()
	{
		System.out.println("ShowCardDetails"+showcartproduct.getText());
	}
	

}
