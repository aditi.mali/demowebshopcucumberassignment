package mainclass;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import baseclasses.BaseClass;
import pomclasses.BookPage;
import pomclasses.ElectronicsPage;
import pomclasses.GiftCard;
import pomclasses.LoginPage;
import pomclasses.LogoutPage;

public class MainExecution {
	
	WebDriver driver;

	LoginPage login;
	BookPage books;
	ElectronicsPage electronic;
	GiftCard gift;
	LogoutPage logout;
	
	@BeforeClass
	public void beforeClass()
	{
		driver=BaseClass.getWebdriver();

	}
	
	@BeforeMethod
	public void beforeMethod()
	{
		login = new LoginPage(driver);
		books = new BookPage(driver);
		electronic= new ElectronicsPage(driver);
		gift= new GiftCard(driver);
		logout= new LogoutPage(driver);
	}
	

	@Test(priority=0)
	public void verifyUserLoginPage() throws InterruptedException
	{
		login.clickLogin();;
		login.enterEmail("manzmehadi1@gmail.com");
		login.enterPassword("Mehek@110");
		login.submitLogin();
	}
	
	@Test(priority=1)
	public void verifyUserBookPage() throws InterruptedException
	{
		books.clickBooks();
		books.selectBook();
		books.addBook();
	}
	
	@Test(priority=2)
	public void verifyUserElectronicPage() throws InterruptedException
	{
		electronic.clickElectronics();
		electronic.clickCellPhone();
		electronic.clickSmartPhone();
		electronic.showCartDetails();
	}
	
	@Test(priority=3)
	public void verifyUserGiftCartPage() throws InterruptedException
	{
		gift.clickGiftCard();
		gift.selectGiftCard();
		gift.displayGiftCart();
	}
	
	@Test(priority=4)
	public void verifyUserLogoutPage() throws InterruptedException
	{
		logout.clickLogout();
	}
	
	@AfterMethod
	public void afterMethod()
	{
		System.out.println("after method executed successfully");
	}

	@AfterClass
	public void afterClass()
	{
		//driver.quit();
	}

}
