Feature: Validate demo web shop
Scenario: validate the demo web shop functionality.
  
  Given user is on application home page 
  When click on the login button
  And user enter the email "manzmehadi1@gmail.com" in email field
  And user enter the password "Mehek@110" in password field 
  Then click on submit button
  And user should be at home page
  When Click on books
  And Select price from High to low 	
  And Add any two books in cart
  Then user able to see the successfully added to cart message
  When Click on electronics Category
  And Select the Cell phones
  And Add to cart
  And Display the count of items added to the cart.
  Then Click on GiftCart
  And Select Display Four per page 
  And Capture the name and price of one of the  Gift cards displayed  
  Then Click on Logout and capture login Diplayed on Home page
  
  